import overlappingTemplateMatchingTest.AdditionalClasses.hypergeometric.Hypergeometric;
import overlappingTemplateMatchingTest.AdditionalClasses.SpecialFunction;
import overlappingTemplateMatchingTest.AdditionalClasses.OwnClasses.OverlappingTemplateMatchingTest;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Math.*;


/**
 * The values of K, M and N have been chosen such that each sequence to be tested consists of a minimum
 * of 10^6 bits (i.e., n ≥ 10^6 ). Various values of m may be selected, but for the time being, NIST recommends
 * m = 9 or m = 10. If other values are desired, please choose these values as follows:
 * • n ≥ MN.
 * • N should be chosen so that N • (min πi) > 5.
 * • λ = (M-m+1)/2m ≈ 2
 * • m should be chosen so that m ≈ log2 M
 * • Choose K so that K ≈ 2λ. Note that the πi values would need to be recalculated for values of K other than 5.
 */
public class Test {
    public static void main(String[] args) {

//        byte[] B = {1, 1};
//        byte[] seq = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};


        byte [] B = {1,1,1,1,1,1,1,1};
        ArrayList<Byte> s = readFile("seq/seq7.bin");
        byte[] result = new byte[s.size()];
        for(int i = 0; i < s.size(); i++) {
            result[i] = s.get(i).byteValue();
        }

        System.out.println(Arrays.toString(result).length());
        OverlappingTemplateMatchingTest otmt = new OverlappingTemplateMatchingTest(B, result);
//        OverlappingTemplateMatchingTest otmt = new OverlappingTemplateMatchingTest(B, seq);

        int[] v = otmt.calculateV();
        System.out.println(Arrays.toString(v));
        double[] pi = new double[v.length];
        for (int i = 0; i < v.length; i++) {
            pi[i] = countPI(i, otmt.getEta());
        }

        System.out.println(Arrays.toString(pi));

        double hi2 = countHi2(v, pi, otmt.getN());
        System.out.println(hi2);
        double pValue = SpecialFunction.igamc(2.5, hi2 / 2.);
        System.out.println(pValue);

    }

    private static ArrayList<Byte> readFile(String filename) {
        ArrayList<Byte> list = new ArrayList<Byte>() ;
        try {
            File f = new File(filename);
            RandomAccessFile file=new RandomAccessFile(f,"r");
            String s;
            for ( int k = 0; k < f.length(); k++) {
                s = Integer.toBinaryString(file.readByte());
                for (int i = 0; i < s.length(); i++)
                    list.add(Byte.parseByte(String.valueOf(s.charAt(i))));
                file.seek(1);
            }
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static double countPI(double u, double n) {
        double res;
        res = ((n * pow(E, -2. * n)) / pow(2., u)) * Hypergeometric.eval1f1(u + 1., 2., n);
        return res;
    }

    public static double countHi2(int[] v, double[] pi, int N) {
        double res = 0.;
        for (int i = 0; i < v.length; ++i) {
            res += (v[i] - N * pi[i]) * (v[i] - N * pi[i]) / (N * pi[i]);
        }
        if (Double.isNaN(res))
            return 0;
        return res;
    }

}
