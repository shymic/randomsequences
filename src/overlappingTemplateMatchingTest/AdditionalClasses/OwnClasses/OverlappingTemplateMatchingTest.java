package overlappingTemplateMatchingTest.AdditionalClasses.OwnClasses;

import overlappingTemplateMatchingTest.AdditionalClasses.hypergeometric.Hypergeometric;

import static java.lang.Math.*;

/**
 The values of K, M and N have been chosen such that each sequence to be tested consists of a minimum
 of 10^6 bits (i.e., n ≥ 10^6 ). Various values of m may be selected, but for the time being, NIST recommends
 m = 9 or m = 10. If other values are desired, please choose these values as follows:
 • n ≥ MN.
 • N should be chosen so that N • (min πi) > 5.
 • λ = (M-m+1)/2^m ≈ 2
 • m should be chosen so that m ≈ log2 M
 • Choose K so that K ≈ 2λ. Note that the πi values would need to be recalculated for values of K other than 5.
 */
public class OverlappingTemplateMatchingTest {

    private int m;// m = 9 or 10 m = B.length
    private int n;// n = seq.length
    private int M;//M=2^m
    private int N;
    private byte[] B;//template
    private double K; //k=2λ
    private byte[] seq;
    private double lambda;
    private double eta;

    public double getEta() {
        return eta;
    }

    public int getN() {
        return N;
    }

    public OverlappingTemplateMatchingTest( byte[] template, byte[] sequence) {
        this.m = template.length;
        this.seq = sequence;
        this.n = sequence.length;
        M = 1 << this.m;
        N = (int)this.n/M;
        B = template;
        lambda = calculateLambda(M,this.m);
        K = 2.*lambda;
        eta = lambda / 2.;
        System.out.println(n);
    }

    public int[] calculateV( ){
        int[] v = new int[N];
        int counter = 0;
        boolean flag = true;
        for ( int i = 0; i < n; i+=N) {
            for ( int k = i; k+m-1<i+N; k++) {
                flag = true;
                for (int j = k; j < k + m; ++j) {
                    if (B[j-k] != seq[j]){
                        flag = false;
                        break;
                    }
                }
                if ( flag == true){
                    counter++;
                }
            }
            v[counter]++;
            counter=0;
        }
        return v;
    }


    public static int[] calculateV( byte[]seq, byte[]B, int n, int N,  int m){
        int[] v = new int[N/2+1];
        int counter = 0;
        boolean flag = true;
        for ( int i = 0; i < n; i+=N) {
            for ( int k = i; k+m-1<i+N; k++) {
                flag = true;
                for (int j = k; j < k + m; ++j) {
                    if (B[j-k] != seq[j]){
                        flag = false;
                        break;
                    }
                }
                if ( flag == true){
                    counter++;
                }
            }
            v[counter]++;
            counter=0;
        }
        return v;
    }


    public static double calculatePI(double u, double n){
        return ((n* pow(E, -2.*n))/pow(2., u) )* Hypergeometric.eval1f1(u + 1., 2., n);
    }

    public static double calculateHi2(double[] v, double[] pi, int N){
        double res = 0.;
        for ( int i = 0; i < v.length; ++i){
            res += (v[i] - N * pi[i])*(v[i] - N * pi[i]) / (N*pi[i]);
        }
        return res;
    }

    public static double calculateLambda(int M, int m){
        return (M-m+1)/pow(2., m);
    }
}
