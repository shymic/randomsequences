package purposeTest.bareklapp_messi;

import java.util.Arrays;

/**
 * Created by Andrey on 21.10.2015.
 */
public class BareklappMessi {
    // Implementation of Berlekamp-Massey algorithm for calculating linear
    // complexity of binary sequence
    // s = byte array with binary sequence
    // returns Length of LFSR with smallest length which generates s
    // for an example: int L=BerlekampMassey(new byte[] {1,0,1,0,1,1,1,0,1,0})
    //        reference: "Handbook of Applied Cryptography", p201

    public static int BerlekampMassey(byte[] s)
    {
        int L, N, m, d;
        int n=s.length;
        byte[] c=new byte[n];
        byte[] b=new byte[n];
        byte[] t=new byte[n];

        //Initialization
        b[0]=c[0]=1;
        N=L=0;
        m=-1;

        //Algorithm core
        while (N<n)
        {
            d=s[N];
            for (int i=1; i<=L; i++)
                d^=c[i]&s[N-i];            //(d+=c[i]*s[N-i] mod 2)
            if (d==1)
            {
                t=c.clone();
//                Array.Copy(c, t, n);    //T(D)<-C(D)
                for (int i=0; (i+N-m)<n; i++)
                    c[i+N-m]^=b[i];
                if (L<=(N>>1))
                {
                    L=N+1-L;
                    m=N;
                    b=t.clone();
//                    Array.Copy(t, b, n);    //B(D)<-T(D)
                }
            }
            N++;
        }
        return L;
    }
}

